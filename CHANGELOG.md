# Change Log

## 1.1: 2023/09/27

application:
    * NXtomo
        * deprecate NXtomo 'node_name' constructor parameter (and ignore it now if provided)
        * make NXtomo.save 'data_path' parameter mandatory

## 1.0: 2023/08/07

Initial version.

application:
    * NXtomo: application definition for x-ray or neutron tomography raw data.

nxobject:
    * nxdetector: nexus object to represent a detector
    * nxinstrument: nexus object for a collection of the components of the instrument or beamline.
    * nxmonitor: nexus object for monitor of incident beam data.
    * nxobject: base class
    * nxsample: nexus object for information on the sample
    * nxsource: nexus object for neutron or x-ray storage ring/facility.
    * nxtransformations: nexus object for collection of axis-based translations and rotations to describe a geometry

paths: define paths of the different dataset to be used - according to a 'nexus_version'
