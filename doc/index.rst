.. |nxtomo_icon| image:: img/nxtomo_icon.png


==============
|nxtomo_icon|   nxtomo
==============

the goal of this project is to provide a powerful and user friendly API to create and edit [NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html) application

Installation
============

The simplest way to install nxtomomill is to use the pypi wheel provided

.. code-block:: bash

   pip install nxtomo


Tutorials
=========

.. toctree::
   :maxdepth: 2

   tutorials/index.rst


API
===

.. toctree::
   :maxdepth: 4

   api.rst
